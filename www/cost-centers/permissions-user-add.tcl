ad_page_contract {
    Redirect page for adding users to the permissions list.
    
    @author Lars Pind (lars@collaboraid.biz)
    @creation-date 2003-06-13
    @cvs-id $Id: permissions-user-add.tcl,v 1.1.1.1 2005/04/18 21:32:07 cvs Exp $
}

set page_title "Add User"

set context [list [list "permissions" "Permissions"] $page_title]

